module piradio

go 1.20

require (
	github.com/hajimehoshi/go-mp3 v0.3.4
	github.com/hajimehoshi/oto v1.0.1
	github.com/stianeikeland/go-rpio v4.2.0+incompatible
	github.com/stianeikeland/go-rpio/v4 v4.6.0
	golang.org/x/text v0.8.0
)

require (
	github.com/gotk3/gotk3 v0.6.1 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/exp/shiny v0.0.0-20230315142452-642cacee5cc0 // indirect
	golang.org/x/image v0.6.0 // indirect
	golang.org/x/mobile v0.0.0-20230301163155-e0f57694e12c // indirect
	golang.org/x/sys v0.6.0 // indirect
)
