package main

import (
	"context"
	"log"
	"piradio/control"
	"piradio/display"
	"piradio/radio"
	"piradio/utils"

	"github.com/hajimehoshi/oto"
)

var err error

func main() {
	utils.CTX, utils.CANCELFUNC = context.WithCancel(context.Background())
	utils.OtoContext, err = oto.NewContext(44100, 2, 2, 8192)
	if err != nil {
		log.Println(err)
	}
	defer utils.OtoContext.Close()
	utils.Player = utils.OtoContext.NewPlayer()
	defer utils.Player.Close()

	utils.DISPLAY = display.NewLcd()

	utils.CHANNEL = 1
	radio.Play()
	if err := control.ReadKeyPress(); err != nil {
		log.Println(err)
	}

}
