package control

import (
	"bufio"
	"fmt"
	"os"
	"piradio/radio"
	"piradio/utils"
	"strconv"
)

func ReadKeyPress() error {
	play := true
	for {
		reader := bufio.NewReader(os.Stdin)
		char, _, err := reader.ReadRune()
		if err != nil {
			fmt.Println(err)
		}

		// prints the unicode code point of the character
		fmt.Println(char)

		switch char {
		case 'p':
			if play {
				utils.CANCELFUNC()
				play = false
			} else {
				radio.Play()
				play = true
			}
		case '1', '2', '3', '4', '5', '6', '7', '8', '9':
			utils.CANCELFUNC()
			num, _ := strconv.Atoi(string(char))
			utils.CHANNEL = num
			radio.Play()
			play = true
		default:
			radio.Play()
			play = true

		}
	}
	return nil
}
