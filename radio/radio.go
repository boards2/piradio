package radio

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"piradio/utils"
	"strings"
	"sync"

	"github.com/hajimehoshi/go-mp3"
)

const (
	bodyFile1Info = "File1="
)

var radio = []string{
	"http://us5.internet-radio.com:8121/",
	"http://us5.internet-radio.com:8125/",
	"http://sr2.inmystream.info:8032/listen.pls?sid=2",
	"http://uk4.internet-radio.com:8049/",
	"http://komplex2.psyradio.org:8010/listen.pls?sid=1",
	"http://us5.internet-radio.com:8201/",
	"http://us2.internet-radio.com:8023/",
	"https://bookradio.hostingradio.ru:8069/fm?c7a2",
	"http://205.234.238.42:4200/live",
}

func urlsSreamAdress(url string) string {
	r, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}
	if r.Header.Get("Content-Type") == "audio/x-scpls" {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			log.Println(err)
		}
		scanner := bufio.NewScanner(strings.NewReader(string(body)))
		for scanner.Scan() {
			line := scanner.Text()
			if strings.Contains(line, bodyFile1Info) {
				return strings.TrimPrefix(line, bodyFile1Info)
			}
		}
	} else {
		return url
	}
	return url

}

func Play() error {
	utils.DISPLAY.Display("Loading buffer\n  ")
	utils.CTX, utils.CANCELFUNC = context.WithCancel(context.Background())
	//func Play(url string, disp display.Display, ctx context.Context) error {
	u := urlsSreamAdress(radio[utils.CHANNEL-1])
	resp, err := http.Get(u)
	if err != nil {
		return err
	}

	Mp3Decoder, err := mp3.NewDecoder(io.Reader(resp.Body))
	if err != nil {
		return err
	}

	name := fmt.Sprintf("Name: %s\n", resp.Header.Get("Icy-Name"))
	genre := fmt.Sprintf("Genre: %s\n", resp.Header.Get("Icy-Genre"))
	utils.DISPLAY.Display(name + genre)

	r := NewReader(utils.CTX, Mp3Decoder) // wrap io.Reader to make it context-aware
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		defer wg.Done()
		go io.Copy(utils.Player, r)
	}()
	wg.Wait()
	return nil
}

type readerCtx struct {
	ctx context.Context
	r   io.Reader
}

func (r *readerCtx) Read(p []byte) (n int, err error) {
	if err := r.ctx.Err(); err != nil {
		return 0, err
	}
	return r.r.Read(p)
}

// NewReader gets a context-aware io.Reader.
func NewReader(ctx context.Context, r io.Reader) io.Reader {
	return &readerCtx{ctx: ctx, r: r}
}
