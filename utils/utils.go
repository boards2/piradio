package utils

import (
	"context"
	"github.com/hajimehoshi/oto"
	"piradio/display"
)

var OtoContext *oto.Context
var Player *oto.Player

var CTX context.Context
var CANCELFUNC context.CancelFunc
var CHANNEL int

var DISPLAY display.Display
